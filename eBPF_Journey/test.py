from bcc import BPF
from bcc.utils import printb
from time import sleep, strftime

# define BPF program
prog = """
#include <linux/sched.h>

struct key_t {    //哈希键定义
    u32 pid;
};

struct data_t {   //哈希值定义
    char comm[TASK_COMM_LEN];
};

BPF_HASH(lat_hash, struct key_t, struct data_t, 512); //定义名为lat_hash的哈希MAP

int hello(struct pt_regs *ctx) {
    struct key_t  key  = {};  //声明键和值
    struct data_t data = {};

    
    key.pid = bpf_get_current_pid_tgid();  //读取当前运行进程的进程号
    bpf_get_current_comm(&data.comm, sizeof(data.comm));  //读取当前运行进程的进程名
    lat_hash.update(&key, &data);   //将数据更新到哈希MAP

    return 0;
}
"""

b = BPF(text=prog) #表面需要挂载eBPF程序所在位置
b.attach_kprobe(event="finish_task_switch", fn_name="hello")   #表面eBPF程序中的hello函数将被挂载到finish_task_switch内核函数上

print("%-8s %-16s" % ("COMM", "PID"))  #打印信息提示
print("Tracing... Hit Ctrl-C to end.")

lat_hash = b.get_table("lat_hash")  #获取哈希MAP数据

while (1):
    sleep(1)

    for k, v in lat_hash.items(): #循环输出哈希MAP中的数据
        print("%8d %16s" % (k.pid, v.comm))

